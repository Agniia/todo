<?php
namespace DBManager; 

class DBManager
{
	protected $host;
	protected $user;
	protected $pass;
	protected $db;
	protected $dbHandler; 
	
	function __construct($host, $user, $pass, $db)
	{
		$this->host = $host;
		$this->user =  $user;
		$this->pass = $pass;
		$this->db = $db;
		$this->doConnect();
	}
	
	protected function doConnect()
	{
		try
		{
			$this->dbHandler = new \PDO('mysql:host='.$this->host.';dbname='.$this->db, $this->user, $this->pass);
		 	$this->dbHandler->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
		 	$this->dbHandler->exec("set names utf8");
	 	}
	    catch (PDOException $e) 
	    {
			 print "Error!: " . $e->getMessage() . "<br/>";
			 die();
		}
	}
	
	public function getDbHandler()
	{
		$this->doConnect();
		return $this->dbHandler;
	}
	
	public function createTODOTables()
	{
		$this->createTableTasks();
		$this->createTableUsers();
	}
	
	protected function createTableTasks()
	{
		try {
			// $sql ="DROP TABLE IF EXISTS `tasks`";
			 $sql ="CREATE TABLE IF NOT EXISTS `tasks` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `user_id` int(11) NOT NULL,
				  `assigned_user_id` int(11) DEFAULT NULL,
				  `description` text NOT NULL,
				  `is_done` tinyint(4) NOT NULL DEFAULT '0',
				  `date_added` datetime NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";	
			 $res = $this->dbHandler->exec($sql);
		 	} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	protected function createTableUsers()
	{
		try {
			// $sql ="DROP TABLE IF EXISTS `users`";
			 $sql ="CREATE TABLE IF NOT EXISTS `users`(
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `login` varchar(50) NOT NULL,
			  `password` varchar(255) NOT NULL,
			  PRIMARY KEY (`id`)
			  )  ENGINE=InnoDB DEFAULT CHARSET=utf8";	
			 $res = $this->dbHandler->exec($sql);
		 	} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	public function getHashedPassword($password)
	{
		$hash = md5($password); 
		return $hash;
	}
	
	public function setNewDataToUsers($login, $password) 
	{
		if($this->checkHaveLogin($login)){
			return null;
		}// die('Пользователь с таким login уже существует');
		$passwordMD5 = $this->getHashedPassword($password);
		try {
			$stmt = $this->dbHandler->prepare("INSERT INTO users (login, password) VALUES (:login, :password)");
			$stmt->bindParam(':login', $login, \PDO::PARAM_STR);
			$stmt->bindParam(':password', $passwordMD5, \PDO::PARAM_STR);
		    $res = $stmt->execute();
		    return $this->dbHandler->lastInsertId(); 
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	public function setNewDataToTasks($user_id, $assigned_user_id,$description,$is_done,$date_added) 
	{	
		try {
			$stmt = $this->dbHandler->prepare("INSERT INTO tasks (user_id, assigned_user_id, description, is_done, date_added) VALUES (:user_id, :assigned_user_id, :description, :is_done, :date_added)");
			$stmt->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
			$stmt->bindParam(':assigned_user_id', $assigned_user_id, \PDO::PARAM_INT);
			$stmt->bindParam(':description', $description, \PDO::PARAM_STR);
			$stmt->bindParam(':is_done', $is_done, \PDO::PARAM_INT);
			$stmt->bindParam(':date_added', $date_added, \PDO::PARAM_STR);
		    $res = $stmt->execute();
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	public function changeTaskStatus($task_id,$task_done) 
	{
		$id = (int)$task_id;
		$is_done = (int)$task_done;
		try {
			$stmt = $this->dbHandler->prepare("UPDATE tasks SET is_done = :is_done WHERE id = :id");
			$stmt->bindParam(':id', $id, \PDO::PARAM_INT);
			$stmt->bindParam(':is_done', $is_done, \PDO::PARAM_INT);
		    $res = $stmt->execute();
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	public function changeTaskResponsible($user_id,$assigned_usr_id) 
	{
		$id = (int)$user_id;
		$assigned_user_id = (int)$assigned_usr_id;
		try {
			$stmt = $this->dbHandler->prepare("UPDATE tasks SET assigned_user_id = :assigned_user_id WHERE id = :id");
			$stmt->bindParam(':id', $id, \PDO::PARAM_INT);
			$stmt->bindParam(':assigned_user_id', $assigned_user_id, \PDO::PARAM_INT);
		    $res = $stmt->execute();
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
		
	public function deleteTask($id) 
	{
		try {
			$stmt = $this->dbHandler->prepare("DELETE FROM tasks WHERE id = :id");
			$stmt->bindParam(':id', $id, \PDO::PARAM_INT);
		    $res = $stmt->execute();
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	public function checkHaveLogin($login) 
	{
	    try {
			$stmt = $this->dbHandler->prepare("SELECT * FROM users WHERE login = :login");
			$stmt->bindParam(':login', $login, \PDO::PARAM_STR);
		    $res = $stmt->execute();
		    if($stmt->rowCount() > 0) return true;
		    else return false;
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
    public function getUser($login) 
	{
	    try {
		    $sql = "SELECT * FROM users WHERE login = '$login' ";
			return $this->dbHandler->query($sql);
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	public function getAllTasks() 
	{
	    $sql = 'SELECT * FROM tasks';
		return $this->dbHandler->query($sql);
	}
		
	public function getAllTasksWithResponsible($user_id) 
	{
	    $sql = 'SELECT tasks.id as task_id, tasks.is_done as status, tasks.description as task_description, tasks.date_added, tasks.user_id, tasks.assigned_user_id, owners.login as owner_login,  responsibles.login as responsible_login,  responsibles.id as responsible_id  FROM tasks 
			JOIN users as owners ON tasks.user_id = owners.id 
			JOIN users as responsibles ON tasks.assigned_user_id = responsibles.id
			WHERE tasks.user_id = '.$user_id;
		return $this->dbHandler->query($sql);
	}
				
	public function getAllTasksWhereOwnerResponsible($id_user) 
	{
	   $assigned_user_id = (int)$id_user;
	    $sql = 'SELECT tasks.id as task_id, tasks.is_done as status, tasks.description as task_description, tasks.date_added, tasks.user_id, tasks.assigned_user_id, owners.login as owner_login,  responsibles.login as responsible_login,  responsibles.id as responsible_id  FROM tasks 
			JOIN users as owners ON tasks.user_id = owners.id 
			JOIN users as responsibles ON tasks.assigned_user_id = responsibles.id
			WHERE tasks.assigned_user_id = '.$assigned_user_id;
		return $this->dbHandler->query($sql);
	}
	
	public function getAllUsers() 
	{
	    $sql = 'SELECT * FROM users';
		return $this->dbHandler->query($sql);
	}
}