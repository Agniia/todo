<h2>Задачи, поставленные Вами<h2>
  <table>
    <tr>
        <td>Задача</td>
        <td>Кода поставлена</td>
        <td>Кем поставлена</td>
        <td>Ответственный</td>
        <td>Статус</td>
        <td>Изменить</td>
    </tr>
    
     <?php  
       $tasksWithResponsible = $dbInstance->getAllTasksWithResponsible($user_id);
       $tasksWithOwnerResponsible = $dbInstance->getAllTasksWhereOwnerResponsible($user_id);
     foreach($tasksWithResponsible as $task)
      { ?>
  	     <tr>
            <td><?php echo $task['task_description']; ?></td>
            <td><?php echo $task['date_added']; ?></td>
            <td><?php echo $task['owner_login']; ?></td>
            <td><?php 
                     if(isset($_POST['changeTask_'.$task['task_id']]))
                      {
                                $selectResponsible = '<form method="post">';
                                $selectResponsible .= '<select name="'.$task['responsible_id'].'_responsibleUserOption">';
                                foreach($tasksWithUsers as $user)
                                {
                                   $selectResponsible .=  '<option value="'.$user['id'].'_'.$user['login'].'">'.$user['login'].'</option>';
                                }
                                $selectResponsible .=  '</select>';
                                $selectResponsible .=  '<button name="makeResponse" value="'.$task['task_id'].'">Сделать ответственным</button>';
                                $selectResponsible .=  '</form>';
                            echo $selectResponsible;    
                      }
                     else if(isset($_POST[$task['responsible_id'].'_responsibleUserOption']) &&
                     $task['task_id'] ==  $_POST['makeResponse'])
                     {
                            $assigned_user_details = explode('_',$_POST[$task['responsible_id'].'_responsibleUserOption']);
                            $dbInstance->changeTaskResponsible($task['task_id'],$assigned_user_details[0]); 
                            header('Location: index.php');
                            $assigned_user_login = $assigned_user_details[1];
                            echo $assigned_user_login;
                     }
                   else
                   {
                            echo $task['responsible_login']; 
                    }            
            ?></td>
            <td><?php 
                    if(isset($_POST['changeTask_'.$task['task_id']]))
                      {
                                $selectStatus = '<form method="post">';
                                $selectStatus .= '<select name="'.$task['task_id'].'_statusOption">';
                                $selectStatus .=  '<option value="'.$task['task_id'].'_0">Не сделано</option>';
                                $selectStatus .=  '<option value="'.$task['task_id'].'_1">Сделано</option>';
                                $selectStatus .=  '</select>';
                                $selectStatus .=  '<button name="changeStatus" value="'.$task['task_id'].'">Изменить статус</button>';
                                $selectStatus .=  '</form>';
                                echo $selectStatus;    
                      }
                     else if(isset($_POST[$task['task_id'].'_statusOption']) &&
                     $task['task_id'] ==  $_POST['changeStatus'])
                     {
                            $status_details = explode('_',$_POST[$task['task_id'].'_statusOption']);                         
                            $dbInstance->changeTaskStatus($task['task_id'],$status_details[1]); 
                            header('Location: index.php');
                            $status = ($status_details[1] == 1) ? 'Сделано' : 'Не сделано';
                            echo $status;
                     }
                   else
                   {
                            $status = ($task['status'] == 1) ? 'Сделано' : 'Не сделано';
                            echo $status;
                    }            
            ?></td>
            <td><form method="post"><button type="submit" value="<?php echo $task['task_id']; ?>" name="changeTask_<?php echo $task['task_id']; ?>">Изменить</button></form></td>
        </tr>
   <?php    }
    ?>
</table>
 <h2>Задачи, поставленные Вам<h2>
  <table>
    <tr>
        <td>Задача</td>
        <td>Кода поставлена</td>
        <td>Кем поставлена</td>
        <td>Ответственный</td>
        <td>Статус</td>
    </tr>
      <?php 
      foreach($tasksWithOwnerResponsible as $taskOR)
      { ?>
            <tr>
                <td><?php echo $taskOR['task_description']; ?></td>
                <td><?php echo $taskOR['date_added']; ?></td>
                <td><?php echo $taskOR['owner_login']; ?></td>
                <td><?php echo $taskOR['responsible_login']; ?></td>
                <td>
                <?php 
                    if(isset($_POST['changeOwnerTask_'.$taskOR['task_id']]))
                      {
                                $selectStatus = '<form method="post">';
                                $selectStatus .= '<select name="'.$taskOR['task_id'].'_statusOptionOwner">';
                                $selectStatus .=  '<option value="'.$taskOR['task_id'].'_0">Не сделано</option>';
                                $selectStatus .=  '<option value="'.$taskOR['task_id'].'_1">Сделано</option>';
                                $selectStatus .=  '</select>';
                                $selectStatus .=  '<button name="changeStatusOwner" value="'.$taskOR['task_id'].'">Изменить статус</button>';
                                $selectStatus .=  '</form>';
                                echo $selectStatus;    
                      }
                     else if(isset($_POST[$taskOR['task_id'].'_statusOptionOwner']) &&
                     $taskOR['task_id'] ==  $_POST['changeStatusOwner'])
                     {
                            $status_details = explode('_',$_POST[$taskOR['task_id'].'_statusOptionOwner']);                         
                            $dbInstance->changeTaskStatus($taskOR['task_id'],$status_details[1]); 
                            header('Location: index.php');
                            $status = ($status_details[1] == 1) ? 'Сделано' : 'Не сделано';
                            echo $status;
                     }
                   else
                   {
                            $status = ($taskOR['status'] == 1) ? 'Сделано' : 'Не сделано';
                            echo $status;
                    }     
                ?>
              </td>
            <td><form method="post"><button type="submit" value="<?php echo $task['task_id']; ?>" name="changeOwnerTask_<?php echo $taskOR['task_id']; ?>">Изменить</button></form></td>
        </tr>
     <?php    
      }
    ?>
</table>